﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CatalogBooks.Models
{
    public class EFContext : DbContext
    {
		public DbSet<Book> Books { get; set; }
		public DbSet<Category> Categories { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=CatalogBooksDB;Trusted_Connection=True;MultipleActiveResultSets=True");
		}
	}
}
