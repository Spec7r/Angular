﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogBooks.Models
{
	public class Book
	{
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public int CategoryId { get; set; }

		public virtual Category Category { get; set; }
	}

}
