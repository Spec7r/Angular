﻿using CatalogBooks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogBooks
{
    public class DataContainer
    {
		private static DataContainer _instance;

		public static DataContainer Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new DataContainer();
				}
				return _instance;
			}
		}

		public IList<Book> GetBookCollection()
		{
			using (var c = new EFContext())
			{
				return c.Set<Book>().Include(i => i.Category).ToList();
			}
		}

		public IList<Category> GetCategoryCollection()
		{
			using (var c = new EFContext())
			{
				return c.Set<Category>().ToList();
			}
		}
	}
}
