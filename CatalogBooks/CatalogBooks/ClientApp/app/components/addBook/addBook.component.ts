﻿import { Observable } from 'rxjs/Observable';
import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers, URLSearchParams } from '@angular/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
    selector: 'addBook',
    templateUrl: './addBook.component.html',
})

export class AddBookComponent {
    public categories: Category[];
    public infoBook: InfoBook; 

    constructor(private http: Http, @Inject('BASE_URL') private  baseUrl: string) {
        http.get(baseUrl + 'api/Data/GetCategory').subscribe(result => {
            this.categories = result.json() as Category[];
        }, error => console.error(error));
        this.infoBook = new InfoBook();
    }

    add() {
        if (!this.infoBook.name && !this.infoBook.categoryId)
        {
            alert('Введите имя и выберите жанр');
            return;
        }
        else if (!this.infoBook.name)
        {
            alert('Введите имя');
            return;
        }
        else if (!this.infoBook.categoryId)
        {
            alert('Выберите жанр');
            return;
        }

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        var param = new URLSearchParams();
        param.set('name', this.infoBook.name);
        param.set('categoryId', this.infoBook.categoryId);
        this.infoBook.categoryId
        this.http
            .post(this.baseUrl + 'api/Data/SaveBook',
            param, {
                headers: headers
            })
            .subscribe(data => {
                alert('Книга успешно добавлена');
                this.infoBook.name = "";
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
        
    }
}

class Category {
    id: number;
    name: string;
}

class InfoBook {
    name: string;
    categoryId: string;
}