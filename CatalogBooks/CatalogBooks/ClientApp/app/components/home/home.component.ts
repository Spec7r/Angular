﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers, URLSearchParams } from '@angular/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    public booksAndCategories: BookAndCategory[];

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
        http.get(baseUrl + 'api/Data/GetInformationBook').subscribe(result => {
            this.booksAndCategories = result.json() as BookAndCategory[];
        }, error => console.error(error));
    }

    delete(bookId: string) {
        let qwe = bookId;
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        var param = new URLSearchParams();
        param.set('bookId', bookId);

        this.http
            .post(this.baseUrl + 'api/Data/DeleteBook',
            param, {
                headers: headers
            })
            .subscribe(data => {
                this.http.get(this.baseUrl + 'api/Data/GetInformationBook').subscribe(result => {
                    this.booksAndCategories = result.json() as BookAndCategory[];
                }, error => console.error(error));
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
    }
}

class BookAndCategory {
    bookId: number;
    categoryId: number;
    bookName: string;
    categoryName: string;
}
