﻿import { Observable } from 'rxjs/Observable';
import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Component({
    selector: 'editBook',
    templateUrl: './editBook.component.html',
})
export class EditBookComponent{
    public id: string;
    public name: string;
    public categoryId: string;
    public categories: Category[];

    constructor(private activateRoute: ActivatedRoute, private http: Http, @Inject('BASE_URL') private baseUrl: string) {
        http.get(baseUrl + 'api/Data/GetCategory').subscribe(result => {
            this.categories = result.json() as Category[];
        }, error => console.error(error));

        this.id = activateRoute.snapshot.params['id'];
        this.name = activateRoute.snapshot.params['name'];
        this.categoryId = activateRoute.snapshot.params['categoryId'];
    }

    edit()
    {
        if (!this.name && !this.categoryId) {
            alert('Введите имя и выберите жанр');
            return;
        }
        else if (!this.name) {
            alert('Введите имя');
            return;
        }
        else if (!this.categoryId) {
            alert('Выберите жанр');
            return;
        }

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        var param = new URLSearchParams();
        param.set('id', this.id);
        param.set('name', this.name);
        param.set('categoryId', this.categoryId);

        this.http
            .post(this.baseUrl + 'api/Data/EditBook',
            param, {
                headers: headers
            })
            .subscribe(data => {
                alert('Книга отредактирована');
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
    }
}

class Category {
    id: number;
    name: string;
}