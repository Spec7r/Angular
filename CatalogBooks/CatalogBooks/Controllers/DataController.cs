﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CatalogBooks.Models;

namespace CatalogBooks.Controllers
{
	[Route("api/[controller]")]
	public class DataController : Controller
	{
		#region methods

		[HttpPost("[action]")]
		public void EditBook(InfoBook obj)
		{
			var infoBook = obj;

			int id = default(int);
			string name = default(string);
			int categoryId = default(int);

			var resultId = Int32.TryParse(infoBook.Id, out id);
			name = obj.Name;
			var resultCategoryId = Int32.TryParse(infoBook.CategoryId, out categoryId);

			if (resultId == false || resultCategoryId == false || string.IsNullOrEmpty(name)) return;

			using (var context = new EFContext())
			{
				var book = DataContainer.Instance.GetBookCollection().Where(b => b.Id == id).FirstOrDefault();

				if (book == null) return;

				book.Category = null;
				book.Name = name;
				book.CategoryId = categoryId;
				context.Books.Update(book);

				context.SaveChanges();
			}
		}

		[HttpGet("[action]")]
		public List<InfoBook> GetBook(string id)
		{
			var infoBook = new List<InfoBook>();
			int idBook = default(int);

			var result = Int32.TryParse(id, out idBook);

			if (result == false) return null;

			var book = DataContainer.Instance.GetBookCollection().Where(b => b.Id == idBook).FirstOrDefault();

			if (book == null) return null;

			infoBook.Add(new InfoBook() { Name = book.Name, CategoryId = book.CategoryId.ToString() });

			return infoBook;
		}

		[HttpGet("[action]")]
		public List<Category> GetCategory()
		{
			return DataContainer.Instance.GetCategoryCollection().OrderBy(b => b.Id).ToList();
		}

		[HttpGet("[action]")]
		public List<BooksAndCategory> GetInformationBook()
		{
			var listOfBooks = new List<BooksAndCategory>();
			var bookCollection = DataContainer.Instance.GetBookCollection().OrderBy(b => b.Id).ToList();

			bookCollection.ForEach(b => listOfBooks.Add(new BooksAndCategory() { BookId = b.Id, CategoryId = b.Category.Id, BookName = b.Name, CategoryName = b.Category.Name }));

			return listOfBooks;
		}

		[HttpPost("[action]")]
		public void SaveBook(InfoBook obj)
		{
			var infoBook = obj;

			if (infoBook is CatalogBooks.Controllers.InfoBook && infoBook != null && !string.IsNullOrEmpty(infoBook.Name) && !string.IsNullOrEmpty(infoBook.CategoryId))
			{
				int id = default(int);

				var result = Int32.TryParse(infoBook.CategoryId, out id);

				if (result == false) return;

				using (var context = new EFContext())
				{ 
					var book = new Book() { Name = infoBook.Name, CategoryId = id };
					context.Books.Add(book);
					context.SaveChanges();
				}
			}
		}

		[HttpPost("[action]")]
		public void DeleteBook(string bookId)
		{
			int id = default(int);

			var result = Int32.TryParse(bookId, out id);

			if (result == false) return;

			using (var context = new EFContext())
			{
				var book = DataContainer.Instance.GetBookCollection().Where(b => b.Id == id).FirstOrDefault();

				if (book == null) return;

				context.Books.Remove(book);
				context.SaveChanges();
			}
		}

		#endregion
	}

	#region Supporting Class
	public class BooksAndCategory
	{
		public int BookId { get; set; }
		public int CategoryId { get; set; }
		public string BookName { get; set; }
		public string CategoryName { get; set; }
	}

	public class InfoBook
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string CategoryId { get; set; }
	}
	#endregion
}
